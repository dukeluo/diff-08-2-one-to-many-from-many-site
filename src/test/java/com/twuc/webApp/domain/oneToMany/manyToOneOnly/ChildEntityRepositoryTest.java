package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        ParentEntity parent = new ParentEntity("parent");
        ChildEntity child = new ChildEntity("child");

        flushAndClear(em -> {
            child.setParentEntity(parent);
            childEntityRepository.save(child);
            parentEntityRepository.save(parent);
        });

        assertTrue(childEntityRepository.findById(child.getId()).isPresent());
        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ParentEntity parent = new ParentEntity("parent");
        ChildEntity child = new ChildEntity("child");

        flushAndClear(em -> {
            child.setParentEntity(parent);
            childEntityRepository.save(child);
            parentEntityRepository.save(parent);
        });

        flushAndClear(em -> {
            ChildEntity savedChild = childEntityRepository.findById(child.getId()).get();

            savedChild.setParentEntity(null);
            childEntityRepository.save(savedChild);
        });


        assertTrue(childEntityRepository.findById(child.getId()).isPresent());
        assertTrue(parentEntityRepository.findById(parent.getId()).isPresent());
        assertNull(childEntityRepository.findById(child.getId()).get().getParentEntity());
        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ParentEntity parent = new ParentEntity("parent");
        ChildEntity child = new ChildEntity("child");

        flushAndClear(em -> {
            child.setParentEntity(parent);
            childEntityRepository.save(child);
            parentEntityRepository.save(parent);
        });

        flushAndClear(em -> {
            parentEntityRepository.deleteById(parent.getId());
            childEntityRepository.deleteById(child.getId());
        });

        assertFalse(childEntityRepository.findById(child.getId()).isPresent());
        assertFalse(parentEntityRepository.findById(parent.getId()).isPresent());
        // --end-->
    }

}